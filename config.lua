return {
    {nom = "Animaux", folder = "animaux", description = "Des animaux génériques et communs (ou moins communs)", descriptionPage = "Cette page comporte les animaux génériques et communs qu'on peut voir tout les jours, plus quelques animaux plus grand que d'habitude. Ces créatures vont être généralement de niveaux plus faible que d'autres que vous pourrez rencontrer plus tard dans le jeu (apres une vache ça reste dangereux hein)"},
    {nom = "Démons et enfer", folder = "demons", description = "Créatures des enfers et satanistes", descriptionPage = "Cette page comporte les créatures et base de PNJ lié aux enfers. Cela peut contenir des démons, des satanistes, ou d'autres créatures lié aux enfers. Ces êtres sont souvent lié à l'élément *chaos*."},
    
}