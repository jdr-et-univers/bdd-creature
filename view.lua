require "libs"
local config = require "config"
local BeastFile = require "classes.beastfile"

local beast = BeastFile("data", arg[1] .. ".beast", tonumber(arg[2]))

local creature = beast:getRawData()

print(creature.name, "Lvl:" .. creature.level, "Pression:" .. creature.pression, creature["type"], creature.categorie)
print(" ", "------")
print("FOR:" .. creature.stats["for"], "INT:" .. creature.stats.int, "CHA:" .. creature.stats.cha)
print("CON:" .. creature.stats.con, "SAG:" .. creature.stats.sag, "DIS:" .. creature.stats.dis)
print("HAB:" .. creature.stats.hab, "VOL:" .. creature.stats.vol, "REL:" .. creature.stats.rel)
print("  ", "PER:" .. creature.stats.per)
print(" ", "------")
print("PV:" .. creature.vita.pv, "PE:" .. creature.vita.pe, "Eclat:" .. creature.vita.eclat)
print("Armure", "Phy:" .. creature.armure.phy, "Psy:" .. creature.armure.psy, "Spe:" .. creature.armure.spe)
print(" ", "------")
print("Armes:")
for key, value in ipairs(creature.armes) do
    print(value.nom, value.force)
end
print(" ", "------")
print("Competences:")
for key, value in ipairs(creature.competence) do
    print(value.name, value.value)
end
print(" ", "------")
print("Skills:")
for key, value in ipairs(creature.skill) do
    print(value[1], value[2])
end