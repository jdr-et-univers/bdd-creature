require "libs"
local config = require "config"
local FolderLoader = require "classes.folderloader"

local json = require "libs.json"

print("Début de génération des fichiers .json")
clock = os.clock()

local bestiaires = {}
local creatures = {}

for _, value in ipairs(config) do
  print("Charagement de la catégorie " .. value.nom)
  FolderLoader.getAllDatas(value, bestiaires, creatures)
end

local file = io.open("build/bestiaires.json", "w")
if (file ~= nil) then
  file:write(json.encode(bestiaires))
end

local file2 = io.open("build/creatures.json", "w")
if (file2 ~= nil) then
  file2:write(json.encode(creatures))
end

clock = os.clock() - clock
print("Génération du fichier .json en " .. clock .. " seconds")