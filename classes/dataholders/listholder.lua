local ListHolder = Object:extend()

function ListHolder:new(key, datas)
    self.key = key
    self.datas = datas
    self.list = {}
end

function ListHolder:applyCommand(command, args)
    if (command == "") then
        self:add(args)
    elseif (command == "reset") then
        self:reset()
    elseif (command == "replace") then
        self:replace(args)
    end
end

function ListHolder:reset()
    self.list = {}
end

function ListHolder:add(datas)
    table.insert(self.list, datas)
end

function ListHolder:replace(datas)
    for index, args in ipairs(self.list) do
        if (args[1] == datas[1]) then
            self.list[index] = datas
        end
    end
end

function ListHolder:reduce(level)
    return self.list
end

return ListHolder