local StatHolder = Object:extend()

function StatHolder:new(key, datas)
    self.key = key
    self.datas = datas
    self.commands = {}
end

function StatHolder:applyCommand(command, args)
    self.commands[command] = args
end

function StatHolder:reduce(level, mode)
    local stat = self.commands.base + ((self.commands.lvl or 0) * level) + (self.commands.add or 0) + (self.commands.bonus or 0)
    local modulo = commands.structs[self.key].modulo or 1
    datas = commands.structs[self.key]
    stat = math.floor(stat / modulo) * modulo
    stat = math.min(stat, datas.max or 999999)
    stat = math.max(stat, datas.min or -99999)
    return stat
end

return StatHolder