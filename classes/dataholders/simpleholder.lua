local SimpleHolder = Object:extend()

function SimpleHolder:new(key, datas)
    self.key = key
    self.datas = datas
    self.value = nil
end

function SimpleHolder:applyCommand(command, args)
    self.value = args
end

function SimpleHolder:reduce(level)
    return self.value
end

return SimpleHolder