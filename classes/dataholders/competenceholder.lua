local CompetenceHolder = Object:extend()
local Competence = Object:extend()

function Competence:new(name)
    self.name = name;
    self.base = 0;
    self.lvl = 0;
    self.bonus = 0;
    self.add = 0;
end

function Competence:reduce(level)
    local baseValue = self.base + (self.lvl * level) + self.bonus + self.add

    return {
        name = self.name,
        value = math.floor(baseValue / 5) * 5
    }
end

function CompetenceHolder:new(key, datas)
    self.key = key
    self.datas = datas
    self.list = {}
end

function CompetenceHolder:applyCommand(command, args)
    if (command == "reset") then
        self.list = {}
        return
    end
    local competence = self:getCompetence(args[1])
    if (command == "") then
        competence.base = tonumber(args[2]) or 0
    elseif (command == "add") then
        competence.add = tonumber(args[2]) or 0
    elseif (command == "bonus") then
        competence.bonus = tonumber(args[2]) or 0
    elseif (command == "lvl") then
        competence.lvl = tonumber(args[2]) or 0
    end
end

function CompetenceHolder:getCompetence(name)
    if self.list[name] == nil then
        self.list[name] = Competence(name)
    end
    return self.list[name]
end

function CompetenceHolder:reduce(level)
    local list = {}
    for key, value in pairs(self.list) do
        table.insert(list, value:reduce(level))
    end
    return list
end

return CompetenceHolder