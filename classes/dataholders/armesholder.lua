local ArmesHolder = Object:extend()

local function applyBoost(level, mode, value)
    return math.floor(value * boosts.getBoost(level, mode))
end

function ArmesHolder:new(key, datas)
    self.key = key
    self.datas = datas
    self.list = {}
end

function ArmesHolder:applyCommand(command, args)
    if (command == "") then
        self:add(args)
    elseif (command == "reset") then
        self:reset()
    elseif (command == "replace") then
        self:replace(args)
    end
end

function ArmesHolder:reset()
    self.list = {}
end

function ArmesHolder:add(datas)
    table.insert(self.list, {nom = datas[1], force = tonumber(datas[2]) or "0"})
end

function ArmesHolder:replace(datas)
    for index, args in ipairs(self.list) do
        if (args.nom == datas[1]) then
            self.list[index] = {nom = datas[1], force = tonumber(datas[2] or "0")}
        end
    end
end

function ArmesHolder:reduce(level, mode)
    for key, value in pairs(self.list) do
        value.force = applyBoost(level, mode, value.force)
    end
    return self.list
end

return ArmesHolder