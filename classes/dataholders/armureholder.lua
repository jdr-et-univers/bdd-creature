local ArmureHolder = Object:extend()

function ArmureHolder:new(key, datas)
    self.key = key
    self.datas = datas
    self.commands = {}
end

function ArmureHolder:applyCommand(command, args)
    self.commands[command] = args
end

function ArmureHolder:reduce(level, mode)
    local stat = self:getBase(level, mode) + ((self.commands.lvl or 0) * level) + (self.commands.add or 0) + (self.commands.bonus or 0)
    local modulo = commands.structs[self.key].modulo or 1
    datas = commands.structs[self.key]
    stat = math.floor(stat / modulo) * modulo
    stat = math.min(stat, datas.max or 999999)
    stat = math.max(stat, datas.min or -99999)

    return stat
end

function ArmureHolder:getBase(level, mode)
    return math.floor(self.commands.base * boosts.getBoost(level, mode))
end

return ArmureHolder