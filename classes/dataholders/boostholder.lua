local BoostHolder = Object:extend()

function BoostHolder:new(list)
    self.parent = list
    self.table = {}
end

function BoostHolder:add(name, val)
    if (self.table[name] == nil) then
        self.table[name] = val
    else
        self.table[name] = self.table[name] + val
    end
end

function BoostHolder:getValue(name, val)
    if (name == "pv" or name == "pe") then
        return val * 3
    else
        return val * 5
    end
end

function BoostHolder:apply()
    for key, value in pairs(self.table) do
        self.parent:addToHolder(key, "lvl", self:getValue(key, value))
    end
end

return BoostHolder