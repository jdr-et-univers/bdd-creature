local FolderLoader = Object:extend()
local BeastFile = require "classes.beastfile"
local GroupFile = require "classes.groupfile"

function FolderLoader.getAllDatas(value, bestiaires, creatures)
    local folderLoader = FolderLoader(value)
    folderLoader:getDatas(bestiaires, creatures)
end

function FolderLoader:new(value)
    self.folder = "data/" .. value.folder

    self.data = {}
    self.data.nom = value.nom
    self.data.description = value.description
    self.data.folder = value.folder
    self.data.list = {}

    self.files = {}

    for _, filename in ipairs(utils.scandir(self.folder)) do 
        local file = utils.split(filename, ".", true)
        if (file[2] == "beast") then
            table.insert(self.files, BeastFile(self.folder, filename))
        elseif (file[2] == "group") then
            table.insert(self.files, GroupFile(self.folder, filename))
        else
            print("[WARNING] Unknown extension " .. file[2] .. " for " .. filename)
        end
    end
end

function FolderLoader:prepareJson(simplercreatures, creatures)
    for _, file in ipairs(self.files) do
        file:prepareJson(simplercreatures, creatures, self.data.nom)
    end
end


function FolderLoader:getDatas(bestiaires, creatures)
    local simplercreatures = {}
    self:prepareJson(simplercreatures, creatures)


    local bestiaire = {
        nom = self.data.nom,
        description = self.data.description,
        folder = self.data.folder,
        list = simplercreatures
    }

    table.insert(bestiaires, bestiaire)
end

return FolderLoader