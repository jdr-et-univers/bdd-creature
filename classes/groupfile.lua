local GroupFile = Object:extend()
local BeastFile = require "classes.beastfile"

local parseFile = require "libs.filereader"

function GroupFile:new(folder, name, forceLevel)
    self.filepath = folder .. "/" .. name
    print("Loading " .. self.filepath)
    self.datas = {}

    self:readLines()
end

function GroupFile:readLines()
    self:readAllLines(self.filepath)
end

function GroupFile:readAllLines(path)
    parseFile(path, function (line) self:addLine(line) end)
end

function GroupFile:addLine(line)
    if (utils.startswith(line, "beast;") or utils.startswith(line, "beasts;")) then
        local datas = utils.split(line, ";", true)
        self:addBeast(datas[2], datas[3], datas[4], self:getMixins(datas))
    end
end

function GroupFile:getMixins(datas)
    if (#datas < 4) then
        return {}
    else
        local list = {}
        for index, value in ipairs(datas) do
            if (index > 4) then
                table.insert(list, value)
            end
        end
        return list
    end
end

function GroupFile:addBeast(name, level, baseFile, mixins)
    local beast = BeastFile("data", baseFile .. ".beast", tonumber(level))
    if (#mixins > 0) then
        for index, mixin in ipairs(mixins) do
            beast:loadMixin(mixin)
        end
    end
    
    beast:forceName(name)
    table.insert(self.datas, beast)
end

function GroupFile:prepareJson(simplercreatures, creatures, parent)
    assert(simplercreatures ~= nil)
    assert(creatures ~= nil)
    --self.datas:prepareJson(simplercreatures, creatures, parent)
    for _, beast in ipairs(self.datas) do
        beast:prepareJson(simplercreatures, creatures, parent)
    end
end

return GroupFile