local BeastFile = Object:extend()
local DataList = require "classes.datalist"

local parseFile = require "libs.filereader"

function BeastFile:new(folder, name, forceLevel)
    self.filepath = folder .. "/" .. name
    print("Loading " .. self.filepath)
    self.datas = DataList(forceLevel)

    self:readLines()
end

function BeastFile:readLines()
    self:readAllLines(self.filepath)
end

function BeastFile:readAllLines(path)
    parseFile(path, function (line) self:addLine(line) end)
end

function BeastFile:addLine(line)
    if (utils.startswith(line, "mixin;") or utils.startswith(line, "mixins;")) then
        local mixin = utils.split(line, ";", true)[2]
        self:loadMixin(mixin)
    elseif (utils.startswith(line, "type;") or utils.startswith(line, "types;")) then
        local mixin = utils.split(line, ";", true)[2]
        self:loadMixin("types/" .. mixin)
    else
        self.datas:addLine(line)
    end
end

function BeastFile:loadMixin(mixin)
    local testpath = "data/" .. utils.trim(mixin) .. ".beast"
    if (testpath ~= nil) then
        self:readAllLines(testpath)
    end
end

function BeastFile:forceName(name)
    self.datas:forceName(name)
end

function BeastFile:prepareJson(simplercreatures, creatures, parent)
    assert(simplercreatures ~= nil)
    assert(creatures ~= nil)
    self.datas:prepareJson(simplercreatures, creatures, parent)
end

function BeastFile:getRawData()
    local content = {}
    self.datas:prepareJson({}, content, "Nothing")
    return content[1]
end

return BeastFile