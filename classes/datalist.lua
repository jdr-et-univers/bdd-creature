local RawData = Object:extend()
local DataList = Object:extend()

local ListHolder = require "classes.dataholders.listholder"
local SimpleHolder = require "classes.dataholders.simpleholder"
local StatHolder = require "classes.dataholders.statholder"
local CompetenceHolder = require "classes.dataholders.competenceholder"
local ArmesHolder = require "classes.dataholders.armesholder"
local ArmureHolder = require "classes.dataholders.armureholder"

local BoostHolder = require "classes.dataholders.boostholder"

function RawData.fromLine(line)
    line = utils.removeComment(line)
    if (#line == 0) then
        return nil
    end
    local command = utils.split(line, "|")
    local datas = utils.split(utils.trim(command[1]), ";")
    local args = {}
    name = utils.trim(datas[1])
    for i, v in ipairs(datas) do
        if (i > 1) then
            local str = utils.trim(v)
            if ((str ~= nil and str ~= "") or i < #datas) then
                table.insert(args, str)
            end
        end
    end
    local level = 0
    if (command[2] ~= nil)  then
        levelString = utils.trim(command[2]);
        level = tonumber(levelString) or 0
    end
    return RawData(name, commands.clean(args, name), level)
end

function RawData:new(name, arguments, level)
    self.name = name
    self.arguments = arguments
    self.level = level
end

function RawData:canBeUsed(level)
    return level >= self.level
end

function RawData:getKey()
    return utils.split(self.name, ".", true)[1]
end

function RawData:getCommand()
    return utils.split(self.name, ".", true)[2] or ""
end

function DataList.getHolder(key, value)
    if (value.dataType == "list") then
        return ListHolder(key, value)
    elseif (value.dataType == "comp") then
        return CompetenceHolder(key, value)
    elseif (value.dataType == "stat") then
        return StatHolder(key, value)
    elseif (value.dataType == "armure") then
        return ArmureHolder(key, value)
    elseif (value.dataType == "armes") then
        return ArmesHolder(key, value)
    end
    return SimpleHolder(key, value)
end

function DataList:new(forceLevel)
    self.forcedLevel = forceLevel
    self.forcedName = nil
    self.list = {}
    self.holders = {}
    self.reducedList = {}
    for key, value in pairs(commands.getDefaults()) do
        table.insert(self.list, RawData(key, value, 0))
    end
    for key, struct in pairs(commands.structs) do
        self.holders[key] = DataList.getHolder(key, struct)
    end

    self.boosts = BoostHolder(self)
end

function DataList:addLine(line)
    table.insert(self.list, RawData.fromLine(line))
end

function DataList:forceName(name)
    self.forcedName = name
end

function DataList:addToHolder(key, command, arguments)
    self.holders[key]:applyCommand(command, arguments)
end

function DataList:reduce()
    local level = 0
    local mode = "creature"
    for _, rawdata in ipairs(self.list) do
        if (rawdata.name == "level") then
            level = rawdata.arguments
        end
        if (rawdata.name == "mode") then
            mode = rawdata.arguments
        end
        if (rawdata.name == "boost") then
            self.boosts:add(rawdata.arguments[1], rawdata.arguments[2])
        end
    end


    if (self.forcedLevel ~= nil) then
        level = self.forcedLevel
    end

    for _, rawdata in ipairs(self.list) do
        if (rawdata:canBeUsed(level) and rawdata.name ~= "boost" and rawdata.name ~= "halfboost") then
            self:addToHolder(rawdata:getKey(), rawdata:getCommand(), rawdata.arguments)
        end
    end

    self.boosts:apply()

    for key, holder in pairs(self.holders) do
        self:addToReducedList(key, holder:reduce(level, mode))
    end

    if (self.forcedLevel ~= nil) then
        self.reducedList["level"] = self.forcedLevel
    end

    if (self.forcedName ~= nil) then
        self.reducedList["name"] = self.forcedName
    end
end

function DataList:addToReducedList(key, data)
    local to = commands.structs[key].to or key
    local toSplited = utils.split(to, ".", true)
    if (#toSplited == 1) then
        self.reducedList[to] = data
    else
        local list = self.reducedList
        for i, toPart in ipairs(toSplited) do
            if (i == #toSplited) then
                list[toPart] = data
            else
                if (list[toPart] == nil) then
                    list[toPart] = {}
                end
                list = list[toPart]
            end
        end
    end
end

function DataList:prepareJson(simplercreatures, creatures, parent)
    self:reduce()

    self.reducedList.parent = parent

    table.insert(simplercreatures, {nom = self.reducedList.name, level = self.reducedList.level, nomType = self.reducedList.nomType, categorie = self.reducedList.categorie})
    table.insert(creatures, self.reducedList)
end

return DataList