local boost = {}

local BOOST_ARM_CREATURE = {
    {lvl = 0, value = 1},
    {lvl = 5, value = 1.5},
    {lvl = 10, value = 2},
    {lvl = 15, value = 3},
    {lvl = 20, value = 3.5},
    {lvl = 25, value = 4}
}

local BOOST_ARM_PNJ = {
    {lvl = 0, value = 1},
    {lvl = 6, value = 1.5},
    {lvl = 10, value = 2},
    {lvl = 15, value = 3}
}


local function getBoost(level, table)
    local bestBoost = {lvl = -1, value = 1}
    for _, boost in ipairs(table) do
        if (level >= boost.lvl and boost.lvl > bestBoost.lvl) then
            bestBoost = boost
        end
    end
    return bestBoost.value
end

function boost.getBoost(level, mode)
    if (mode == "pnj") then
        return getBoost(level, BOOST_ARM_PNJ)
    else
        return getBoost(level, BOOST_ARM_CREATURE)
    end
end


return boost