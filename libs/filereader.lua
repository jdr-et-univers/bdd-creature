-- http://lua-users.org/wiki/FileInputOutput

local utils = require "libs.utils"

-- see if the file exists
local function file_exists(file)
  local f = io.open(file, "rb")
  if f then f:close() end
  return f ~= nil
end

-- get all lines from a file, returns an empty 
-- list/table if the file does not exist
local function lines_from(file)
  if not file_exists(file) then error(file) end
  local lines = {}
  for line in io.lines(file) do 
    lines[#lines + 1] = line
  end
  return lines
end

local function parseFile(file, func)
  local lines = lines_from(file)
  for k,v in pairs(lines) do
    local line = utils.trim(v)
    line = utils.trim(utils.split(line, "//", true)[1])
    if (line ~= "") then
      func(line)
    end
  end
end

return parseFile;