local utils = {}

utils.split = require "libs.split"

function utils.trim(s)
   return s:match "^%s*(.-)%s*$"
end

function utils.startswith(string, start)
    return string:sub(1, #start) == start
end

function utils.endswith(string, ending)
    return ending == "" or string:sub(-#ending) == ending
end

function utils.removeComment(line)
  return utils.trim(utils.split(utils.trim(line), '/', true)[1])
end

function utils.scandir(directory)
  local i, t, popen = 0, {}, io.popen
  local pfile = popen('ls -a "'..directory..'"')
  for filename in pfile:lines() do
      if (filename ~= "." and filename ~= "..") then
        i = i + 1
        t[i] = filename
      end
  end
  pfile:close()
  return t
end

function utils.contains(list, value)
  if list == nil then
    return false
  end
  for _, listValue in pairs(list) do
    if (listValue == value) then
      return true
    end
  end
  return false
end

function utils.size(tableToTest)
  if (tableToTest == nil) then
    return 0
  end
  return #tableToTest
end

function utils.slitWithoutWhiteSpace(str, sep, plain)
  local strs = utils.split(str, sep, plain)
  local strsWithoutWhitespace = {}

  for key, value in pairs(strs) do
    table.insert(strsWithoutWhitespace, utils.trim(value))
  end

  return strsWithoutWhitespace
end

function utils.fileExists(name)
   local f=io.open(name,"r")
   if f~=nil then io.close(f) return true else return false end
end

return utils