local struct = require "struct"
local commands = {}
local defaults = {}

local functions = {}

print("Compilation des commandes")

local function addCommands(command, parent, default)
    commands[command] = parent
    if (default ~= nil) then
        defaults[command] = default
    end
end

local function addStatCommands(name, value)
    addCommands(name .. ".base", name, value.default)
    addCommands(name .. ".lvl", name, value.lvl or 0)
    addCommands(name .. ".add", name, 0)
    addCommands(name .. ".bonus", name, 0)
end

local function addCompCommands(name, value)
    addCommands(name, name)
    addCommands(name .. ".reset", name)
    addCommands(name .. ".lvl", name)
    addCommands(name .. ".add", name)
    addCommands(name .. ".bonus", name)
end

local function addListCommands(name, value)
    addCommands(name, name)
    addCommands(name .. ".replace", name)
    addCommands(name .. ".reset", name)
end

for key, value in pairs(struct) do
    if (value.dataType == "stat" or value.dataType == "armure") then
        addStatCommands(key, value)
    elseif (value.dataType == "list") then
        addListCommands(key, value)
    elseif (value.dataType == "comp" or value.dataType == "armes") then
        addCompCommands(key, value)
    else
        addCommands(key, key, value.default)
    end
end

function functions.getDefaults()
    return defaults
end

function functions.clean(args, command)
    local baseCommand = commands[command]
    if (baseCommand == nil) then
        error("Command " .. command .. " doesn't exists")
    end
    local commandData = struct[baseCommand]
    if (commandData.args == nil or commandData.args == 1) then
        local arg = args[1]
        if (commandData.contentType == "number") then
            arg = tonumber(arg)
        end
        return arg
    end
    return args
end

functions.structs = struct

return functions